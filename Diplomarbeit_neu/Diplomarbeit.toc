\select@language {ngerman}
\select@language {ngerman}
\select@language {ngerman}
\select@language {ngerman}
\contentsline {chapter}{Autorenverzeichnis}{x}{chapter*.6}
\contentsline {chapter}{\numberline {1}Theoretische Grundlagen}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Was ist eine elektromagnetische Welle ?}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Elektromagnetische Wellen im Raum}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Aufbau einer Antenne}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Wie funktioniert der Empfang von Wellen?}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}Elektromagnetische Spektrum}{7}{section.1.5}
\contentsline {section}{\numberline {1.6}\IeC {\"U}bertragung der Information}{8}{section.1.6}
\contentsline {section}{\numberline {1.7}Die Modulation}{8}{section.1.7}
\contentsline {section}{\numberline {1.8}Die Bandbreite und die \IeC {\"U}bertragung von Informationen}{9}{section.1.8}
\contentsline {section}{\numberline {1.9}Empfang der elektromagnetischen Wellen}{10}{section.1.9}
\contentsline {section}{\numberline {1.10}Kenngr\IeC {\"o}\IeC {\ss }en einer Antenne}{10}{section.1.10}
\contentsline {subsection}{\numberline {1.10.1}Antennentypen}{11}{subsection.1.10.1}
\contentsline {subsection}{\numberline {1.10.2}Rundumstrahlende Antenne}{11}{subsection.1.10.2}
\contentsline {subsection}{\numberline {1.10.3}Richtantennen}{11}{subsection.1.10.3}
\contentsline {subsection}{\numberline {1.10.4}Sektorantennen}{11}{subsection.1.10.4}
\contentsline {subsection}{\numberline {1.10.5}Der Antennengewinn}{11}{subsection.1.10.5}
\contentsline {subsection}{\numberline {1.10.6}Stecker und Kabelverbindungen}{11}{subsection.1.10.6}
\contentsline {subsection}{\numberline {1.10.7}Polarisation}{12}{subsection.1.10.7}
\contentsline {subsection}{\numberline {1.10.8}Richtcharakteristik}{12}{subsection.1.10.8}
\contentsline {subsection}{\numberline {1.10.9} Reflektionen}{12}{subsection.1.10.9}
\contentsline {subsection}{\numberline {1.10.10}Interferenz}{12}{subsection.1.10.10}
\contentsline {subsection}{\numberline {1.10.11}D\IeC {\"a}mpfung}{12}{subsection.1.10.11}
\contentsline {section}{\numberline {1.11}Richtfunk}{13}{section.1.11}
\contentsline {subsection}{\numberline {1.11.1}Wieso wird Richtfunk verwendet?}{13}{subsection.1.11.1}
\contentsline {subsection}{\numberline {1.11.2}Einsatz von optischen und Mikrowellenrichtfunk}{13}{subsection.1.11.2}
\contentsline {subsection}{\numberline {1.11.3}Definition der Verf\IeC {\"u}gbarkeit}{14}{subsection.1.11.3}
\contentsline {subsection}{\numberline {1.11.4}Redundanzanwendungen}{14}{subsection.1.11.4}
\contentsline {section}{\numberline {1.12}Yagi-Antenne}{16}{section.1.12}
\contentsline {subsection}{\numberline {1.12.1}Berechnung einer Yagi-Antenne}{16}{subsection.1.12.1}
\contentsline {subsection}{\numberline {1.12.2}Beeinflussung durch Witterung}{16}{subsection.1.12.2}
\contentsline {chapter}{\numberline {2}Ziele}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Muss-Ziele}{19}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Lukrierung der Standorte}{20}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Ermittlung des physikalischen Hintergrunds}{20}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Bauen der Antenne}{20}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Installieren der Systeme}{21}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Messungen und Auswertungen durchf\IeC {\"u}hren}{21}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Kann-Ziele}{21}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Optimierung der Systeme}{22}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Nicht-Ziele}{22}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Erstellen einer Bauanleitung}{22}{subsection.2.3.1}
\contentsline {chapter}{\numberline {3}Weg der Durchf\IeC {\"u}hrung}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Ist-Erhebung}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}M\IeC {\"o}glichkeiten der Vernetzung}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Warum eine Yagi-Antenne?}{26}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Standorte}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}TGM}{27}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}UBIS}{29}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Leobendorf}{30}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Arbeitsdurchf\IeC {\"u}hrung}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Lukrierung der Standorte}{31}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}TGM}{31}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}UBIS}{32}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Leobendorf}{35}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Ermitteln des physikalischen Hintergrunds}{35}{section.4.2}
\contentsline {section}{\numberline {4.3}Aufsetzen der Testumgebung}{37}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Bauen der Antenne}{37}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Aufsetzen und Konfigurieren der Router}{46}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Durchf\IeC {\"u}hrung der Messungen}{53}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Erste Testmessung}{54}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Messung bei Schneefall}{54}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Messung bei bew\IeC {\"o}lktem Himmel/leichtem Regen}{56}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Messung bei Sonnenschein}{57}{subsection.4.4.4}
\contentsline {chapter}{\numberline {5}Ergebnis}{59}{chapter.5}
\contentsline {chapter}{\numberline {6}Erweiterungsm\IeC {\"o}glichkeiten}{63}{chapter.6}
\contentsline {section}{\numberline {6.1}Allgemein}{63}{section.6.1}
\contentsline {section}{\numberline {6.2}Standing Wave Ratio}{63}{section.6.2}
\contentsline {section}{\numberline {6.3}Transformation \IeC {\"u}ber ein Kabel}{66}{section.6.3}
\contentsline {section}{\numberline {6.4}Optimierung einer Antenne}{68}{section.6.4}
\contentsline {section}{\numberline {6.5}Wechseln der Frequenz}{68}{section.6.5}
\contentsline {chapter}{\numberline {7}Fazit}{71}{chapter.7}
\contentsline {section}{\numberline {7.1}Verbesserung bereits bestehender Systeme}{71}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Verbesserung der Verkabelung}{71}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Verbesserung der Router}{72}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Wechsel der Antenne}{73}{subsection.7.1.3}
\contentsline {section}{\numberline {7.2}Folgeprojekte}{73}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Ber\IeC {\"u}cksichtigung mehrerer Faktoren}{74}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Vernetzung eines Skigebiets}{74}{subsection.7.2.2}
\contentsline {chapter}{\numberline {A}Zeitaufzeichnungen}{79}{appendix.A}
\contentsline {section}{\numberline {A.1}Maximilian Augustin Ernst}{80}{section.A.1}
\contentsline {section}{\numberline {A.2}Andreas Hoffmann}{82}{section.A.2}
\contentsline {section}{\numberline {A.3}Raphael Kaiser}{84}{section.A.3}
\contentsline {section}{\numberline {A.4}Michael Werner}{86}{section.A.4}
\contentsline {chapter}{\numberline {B}Genehmigung f\IeC {\"u}r das Betreten des TGM-Dachs}{89}{appendix.B}
