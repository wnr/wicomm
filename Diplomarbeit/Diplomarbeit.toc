\select@language {ngerman}
\contentsline {section}{\numberline {1}Vorwort}{1}{section.1}
\contentsline {section}{\numberline {2}Theoretische Grundlagen}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Was ist eine elektromagnetische Welle ?}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Elektromagnetische Wellen im Raum}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Aufbau einer Antenne}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Elektromagnetische Spektrum}{4}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Richtfunk}{5}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Yagi-Antenne}{5}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Beeinflussung durch Witterung}{5}{subsection.2.7}
\contentsline {subsubsection}{\numberline {2.7.1}Beeinflussung der Antenne}{5}{subsubsection.2.7.1}
\contentsline {subsubsection}{\numberline {2.7.2}Beeinflussung der \IeC {\"U}bertragung}{5}{subsubsection.2.7.2}
\contentsline {section}{\numberline {3}Ziele}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Muss-Ziele}{5}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Lukrierung der Standorte}{5}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Ermittlung des physikalischen Hintergrunds}{5}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Bauen der Antenne}{5}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Installieren der Systeme}{6}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Messungen und Auswertungen durchf\IeC {\"u}hren}{6}{subsubsection.3.1.5}
\contentsline {subsection}{\numberline {3.2}Kann-Ziele}{6}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Optimierung der Systeme}{6}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3}Nicht-Ziele}{6}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Erstellen einer Bauanleitung}{6}{subsubsection.3.3.1}
\contentsline {section}{\numberline {4}Weg der Durchf\IeC {\"u}hrung}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Ist-Erhebung}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}M\IeC {\"o}glichkeiten der Vernetzung}{8}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Warum eine Yagi-Antenne?}{9}{subsubsection.4.2.1}
\contentsline {subsection}{\numberline {4.3}Standorte}{9}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}TGM}{10}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}UBIS}{11}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Leobendorf}{11}{subsubsection.4.3.3}
\contentsline {section}{\numberline {5}Arbeitsdurchf\IeC {\"u}hrung}{12}{section.5}
\contentsline {subsection}{\numberline {5.1}Lukrierung der Standorte}{12}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Ermitteln des physikalischen Hintergrunds}{12}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Aufsetzen der Testumgebung}{12}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Bauen der Antenne}{12}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Installieren der Systeme}{12}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Durchf\IeC {\"u}hrung der Messungen}{12}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Messprotokolle}{12}{subsubsection.5.4.1}
\contentsline {subsection}{\numberline {5.5}Auswertung der Messungen}{12}{subsection.5.5}
\contentsline {section}{\numberline {6}Ergebnis}{12}{section.6}
\contentsline {section}{\numberline {7}Erweiterungsm\IeC {\"o}glichkeiten}{12}{section.7}
\contentsline {section}{\numberline {8}Fazit}{14}{section.8}
